<?php

namespace app\models;

use Yii;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $telegram_id;
    public $code;

    private static $user = [

    ];


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $user = Yii::$app->db->createCommand('SELECT * FROM auth WHERE id=:id')->bindValue(':id', $id)->queryOne();
        Yii::$app->db->createCommand()->update('auth', ['code'=>null], 'id = :id')->bindValue('id', $user->id)->execute();
        return isset(self::$user) ? new static($user) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user = Yii::$app->db->createCommand('SELECT * FROM auth WHERE username=:username')->bindValue(':username', $username)->queryOne();
        return new static($user);
    }

    public static function findByCode($code)
    {
        $user = Yii::$app->db->createCommand('SELECT * FROM auth WHERE code=:code')->bindValue(':code', $code)->queryOne();
        return new static($user);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if ($this->password === $password) {
            $token = "1967904268:AAFcaGXZgwImC87EkyKs5UMFonO1fl3vsnM";

            $code = random_int(1000, 9999);
            $data = [
                'chat_id' => $this->telegram_id,
                'text' => $code
            ];
            file_get_contents("https://api.telegram.org/bot$token/sendMessage?" . http_build_query($data));
            $this->code = $code;
            Yii::$app->db->createCommand()->update('auth', ['code'=>$code], 'id = :id')->bindValue('id', $this->id)->execute();
            return true;
        } else {
            return false;
        }
    }
}
