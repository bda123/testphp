<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class CodeForm extends Model
{
    public $code;

    private $_user = false;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['code'], 'required'],
            ['code', 'validateCode']
        ];
    }

    public function validateCode($attribute, $params)
    {
        $dbCode = Yii::$app->db->createCommand('SELECT code FROM auth WHERE code=:code')->bindValue(':code', $this->code)->queryOne();
        if (empty($dbCode)) {
            $this->addError($attribute, 'Incorrect code.');
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
     public function login()
     {
         if ($this->validate()) {
             return Yii::$app->user->login($this->getUser());
         }
         return false;
     }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByCode($this->code);
        }

        return $this->_user;
    }
}
